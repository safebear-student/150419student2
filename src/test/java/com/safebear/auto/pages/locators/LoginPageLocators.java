package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

// using @Data, it inserts the import automatically above.  Newer version of Lombok, use @Getter, will add import .Data and .Getter

@Data
public class LoginPageLocators {
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By remeberMeCheckBox = By.id("remember");
    private By loginButtonLocator = By.id("enter");

 //this will point to th elocation of the box that will contain the message text.  Not required, but next approach is better
   // private By rejectLoginMsgLocator = By.id("rejectLogin");

    // In contract this will point to the actual text for reject login, where th ecode ends with/b
    private By faildLoginMessage = By.xpath(".//p[@id='rejectLogin']/b");
}
