package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class ToolsPageLocators {
    private By loginContainerLocator = By.id("login Successful");
    private By welcomeMessageLocator = By.id("Welcome Tester");
    private By textMessageLocator = By.xpath(".//p(@id='jumbotron'");
    private By searchHereLocator = By.id("toolName");



}
