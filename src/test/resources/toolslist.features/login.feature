Feature: Login
  In order to access the website
  As a user
  I want to know if my login is successful

  Rules:
  * The user must be informed if the login information is incorrect
  * The usee must be informed if the loginb is successful

  Glossary
  * User: Someone who wants to create a Tools List using our application
  * Supporters: This is what the customer calls 'Admin' users.

  @HighRisk
  @HighImpact
  Scenario Outline: Navigate and login to th eapplication
    Given I navigate to the login page
    When I enter the login details for a '<userType>'
    Then I can see the following mesage: '<validationMessage>'
    Examples:
      | userType   | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |